This is a jumbled mess of an application right now, in terms of the source code and general sourcing information. The executable works (written in C, found in cryptor.old), just be wary when editing the source, that it needs to be updated and cleaned up. 

This source code is used as a reference to gauge generally what the program stored in ~/bin does. It may or may not compile and run correctly. As far as I know, this might be the actual source code that created the crypt I used before I realized KeepassXC works better than I could ever imagine, or that there's a long-standing UNIX tool "pass" that does the same thing, also better. 

It's basically Caesar's Cypher, but it uses XOR instead of +6 letters down the alphabet. I made this when I was 14, and it's just something interesting I wanna upload that isn't very useful other than learning purposes.
