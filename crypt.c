#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void help();	// displays the help information
void filecrypt(char*, char*);	// does the file reading, xor encryption, and file writing

int main(int argc, char*argv[]){
	char text[1000];
	char pass[1000];

	if(argc == 1){
		printf("file name: ");
		scanf("%s", text);
		printf("password: ");
		scanf("%s", pass);
	}else if(argc == 3){
		strcpy(text, argv[1]);
		strcpy(pass, argv[2]);
	}else if(argc == 2 && strcmp(argv[1], "--help") == 0){
		help();
		return 0;
	}else{
		printf("Error reading arguments.\n");
		printf("syntax: crypt [file name] [password]\n");

		printf("argc: %d\n", argc);
		printf("argv[1]: [%s]\n", argv[1]);

		return 1;
	}

	filecrypt(text, pass);
	printf("Done!\n");

	return 0;
}

void help(){
	FILE *help;
	char buf[1000];

	help = fopen("READ ME.crypt", "r");
	if(!help)return;

	while(fgets(buf, 1000, help) != NULL)
		printf("%s", buf);

	fclose(help);
}

void filecrypt(char* fname, char* passwd){
	FILE *ofile, *cfile;
	
	char cfname[1000];
	sprintf(cfname, "%s.crypt", fname);

	ofile = fopen(fname, "r");
	cfile = fopen(cfname, "w");

	if(!ofile){
		if(fname == "--help"){
			printf("syntax: crypt [file name] [password]\n");
			return;
		}

		printf("%s doesn't exist as a file. Please follow the syntax below:\n");
		printf("crypt [file name] [password]\n");
		return;
	}
	if(!cfile)return;

	int ochar;
	int cchar;

	int i;
	int psize = strlen(passwd);		// password size
	for(i=0; ochar != EOF; i++){
		ochar = fgetc(ofile);
		cchar = ochar ^ passwd[i % psize];	// xor encryption
		fputc(cchar, cfile);
	}

	fclose(ofile);
	fclose(cfile);
}
